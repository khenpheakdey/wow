import React from 'react';
import ReactLoading from 'react-loading';
const Loading = (props) => {
    const isLoading = props.loading
    if (isLoading) {
        return (
            <div style={{ width: "100%", display: 'flex', justifyContent: "center", alignItems: "center", marginTop: "4rem" }}>
                <ReactLoading type="spinningBubbles" color="#F47A50" height={341} width={150} />
            </div>
        );
    }
    else 
{
    return (null)
}

}

export default Loading;
