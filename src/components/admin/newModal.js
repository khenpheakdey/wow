import React, { useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import axios from "axios"

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));





export default function TransitionsModal() {
  const classes = useStyles();
  let [newEvent, setnewEvent] = useState({
    title: "",
    description: "",
    location: "",
    eventImage: "",
    deadline: "",
    category: [],
    featured: false
  })


  const [open, setOpen] = React.useState(false);
  let [category, setCategory] = useState([{
    name: "Arts",
    isChecked: false
  }, {
    name: "Sports",
    isChecked: false,
  },
  {
    name: "Technology",
    isChecked: false,
  },
  {
    name: "Gaming",
    isChecked: false,
  },
  {
    name: "Dance",
    isChecked: false,
  },
  {
    name: "Music",
    isChecked: false,
  }
  ])


  const [selectCategory, setSelectCategory] = useState([])
  const clickCheck = (e) => {
    let isChecked = e.target.checked;
    if (isChecked) {
      selectCategory.push(e.target.value)
    }
    else {
      let index = selectCategory.findIndex((element) => {
        return element = e.target.value
      })
      selectCategory.splice(index, 1)
    }
    // setnewEvent(prevState => ({
    //   ...prevState,
    //   category: selectCategory.join("&")
    // })
    // )
    // newEvent.category = selectCategory.join("&")
    newEvent.category = selectCategory
    console.log(newEvent.category);
  }


  let [files, setFile] = useState();
  const handleFile = (e) => {
    files = e.target.files[0]
    newEvent.eventImage = files
    console.log(files.name);
  }

  async function postEvent(e) {
    e.preventDefault();
    let data = new FormData();
    data.append('eventImage', newEvent.eventImage);
    data.append('title', newEvent.title);
    data.append('description', newEvent.description);
    data.append('location', newEvent.location)
    data.append('deadline', newEvent.deadline)
    data.append("featured", newEvent.featured)
    data.append("category", newEvent.category)


    const response = await axios.post("https://jisoo-board.herokuapp.com/api/event", data

    )

    console.log(response);

  }



  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <button type="button" onClick={handleOpen}>
        react-transition-group
      </button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <form className={classes.paper} onSubmit={(e) => postEvent(e)} >
            <label>
              Title:
          <input name="title" type="text" value={newEvent.title} onChange={(e) => setnewEvent(prevState => ({ ...prevState, title: e.target.value }))} />
            </label>
            <label>
              Description:
          <input name="description" value={newEvent.description} type="text" onChange={(e) => setnewEvent(prevState => ({ ...prevState, description: e.target.value }))} />
            </label>
            <label>
              Category:
          {category.map((cate) => {
              return (
                <span className="cateogry">
                  <input type="checkbox" name={cate.name} value={cate.name} onClick={(e) => clickCheck(e)} />{cate.name}
                </span>
              )
            })}
            </label>
            <label>

              <input type="file" onChange={(e) => handleFile(e)} />
            </label>
            <input type="submit" value="Submit" />
          </form>
        </Fade>
      </Modal>
    </div>
  );
}