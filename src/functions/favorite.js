import axios from "axios"

async function addFavorite(eventId){
    let userId = JSON.parse(localStorage.getItem("user"))

    if(!userId){
        return false;
    }
    else{
        const response = await axios.post("https://jisoo-board.herokuapp.com/api/wishlist", {
            eventId, 
            userId, 
        }, { headers: {"Authorization" : `Bearer ${localStorage.getItem("token")}`} })

        console.log(response);
        return true;
    }
}


export default addFavorite