import { React, useState } from 'react';
import { useHistory } from 'react-router-dom';
import ReactLoading from 'react-loading'; 

import displayDate from './../../functions/displayDate';

import "../../scss/modal.scss"


const ModalEvent = (props) => {

    const history = useHistory();

    const checkWishlist = (event) => {
        return props.wishList.some(evt => evt._id === event._id)
    }

    const [heartClick, setHeartClick] = useState(false);
    
    let content;
    
    if(!props.isLoading){
        content = props.featureEvent.map(feature => {
            return (
                <div className="event-card">
                    <div className="event-img">
                        <div className="img overlay" style={{backgroundImage: `url(${feature.image})`}} onClick={()=> history.push("/event/" + feature._id)}></div>
                        <button className="add-btn" onClick={(e) => props.handleAddFavorite(e,feature._id)}>
                            <ion-icon name="heart" style={{color: checkWishlist(feature) ? "tomato" : "white"}}></ion-icon>
                        </button>
                    </div>
                    <div className="event-desc">
                        <h3>{feature.title}</h3>
                        <div className="event-details">
                            <div className="event-location">
                                <ion-icon name="location"></ion-icon>
                                <p>{feature.location}</p>
                            </div>
                            <div className="event-bottom">
                                <div className="event-date">
                                    <ion-icon name="calendar"></ion-icon>
                                    <p>{displayDate(feature.deadline)}</p>
                                </div>
                                <div className="category-list">
                                    {feature.category.map(category=>{
                                        return (
                                        <div className="event-category">{category}</div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    } else {
        content = <div className="respond-loading">
            <ReactLoading id="loading" type="spinningBubbles" color="#F47A50" height={341} width={150}/>
        </div>
    }

    return (
        <div className="category-modal">
            {!props.allEvent ? <div className="info-box">
                <div className="info">
                    <div className="info-text">
                        <h1>{props.title}</h1>
                        <div className="info-desc">
                            <p>{props.desc}</p>
                        </div>
                    </div>
                    <button className="discover-btn" onClick={() => history.push({pathname: "/event", state: {type: props.type}})}>
                        <p>DISCOVER </p>
                    </button>
                </div>
            </div> : null
            }
            <div className="event-container">
                <div className="title">
                    <h3>{props.title}</h3>
                    <button className="show-more" onClick={() => history.push({pathname: "/event", state: {type: props.type}})}> 
                        <p> Show more </p>
                        <ion-icon name="chevron-forward-outline"></ion-icon>
                    </button>
                </div>
                <div className="event-content" style={{justifyContent: props.isLoading ? 'center' : 'space-between'}}>
                    {content}
                </div>
            </div>
        </div>
    )
}

export default ModalEvent