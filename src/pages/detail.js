import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import '../scss/detail.scss';
import Calendar from 'react-calendar'
import displayDate from './../functions/displayDate'
import 'react-calendar/dist/Calendar.css';
import ReactLoading from 'react-loading'; 
import { Modal, Button } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";
import addFavorite from './../functions/favorite';
import CannotFound from '../../src/cannotfound.json';
import Lottie from 'react-lottie';


const Detail = () => {
    const history = useHistory();

    let { id } = useParams()
    const [event, setevent] = useState({});
    const [relatedEvent, setrelatedEvent] = useState([])
    const [isLoading,setisLoading] = useState();


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleAddFavorite = async (id) => {
        const status = await addFavorite(id);

        if (status) {
            console.log("success")
        } else {
            handleShow();
        }
    }
    

    const fetchOne = async()=>{
        setisLoading(true);
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/event/one/" + id)
        console.log(response);
        setevent(response.data.event)
        setrelatedEvent(response.data.relatedEvent)
        console.log(response.data.relatedEvent);
        setisLoading(false);
    }

    function NewlineText(tex) {
        const text = tex;
        const newText = text.split('\n').map(str => (str !== "") ? <p>{str}</p> : <br></br>);
        return newText;
    }

    useEffect(() => {
        fetchOne()
    }, []);

    let content = !relatedEvent ? null : relatedEvent.map(relate => {
        return (
            <div className="event-card">
                <img src={relate.image} alt="event poster" onClick={()=> {
                    history.push("/event/" + relate._id);
                    history.go(0);
                }}/>
                <div className="event-desc">
                    <h3>{relate.title}</h3>
                    <div className="event-details">
                        <div className="event-location">
                            <ion-icon name="location"></ion-icon>
                            <p>{relate.location}</p>
                        </div>
                        <div className="event-bottom">
                            <div className="event-date">
                                <ion-icon name="calendar"></ion-icon>
                                <p>{displayDate(relate.deadline)}</p>
                            </div>
                            <div className="category-list">
                                {relate.category}
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    })

    if(relatedEvent.length===0){
        content =  <div className="no-relate">
            <Lottie options={{animationData: CannotFound}} height={200}
                        width={200} style={{margin: 0}}/>
                        <p>Sorry, We cannot find Related Event!</p>
        </div>
    }

    if(!isLoading){
        return (
            <div className="detail-container">
                <div className="detail-header">
                    <div className="event-image">
                        <img src={event.image} alt="event poster" />
                    </div>
                    <div className="event-info">
                        <div className="event-category">
                            <h3>{event.category}</h3>
                        </div>
                        <div className="event-information">
                            <h3>{event.title}</h3>
                            <div className="date-locate">
                                <p><ion-icon name="calendar-outline"></ion-icon>{displayDate(event.deadline)}</p>
                                <p><ion-icon name="location-outline"></ion-icon>{event.location}</p>
                            </div>
                        </div>
                        <div className="line"></div>
                        <div className="event-footer">
                            <button className="add-favorite button" onClick={() => handleAddFavorite(event._id)}>
                                <p>ADD TO FAVORITE</p>
                            </button>
                        </div>
                    </div>
                </div>
                <h2>Description</h2>
                <div className="detail-body">
                    <div className="event-description">
                        <div className="des-line"></div>
                        <p style={{ lineHeight: "1.5", fontSize: "1rem" }}> {event.description ? NewlineText(event.description) : null} </p>
                    </div>
                    <div className="detail-right">
                        <div className="cal-line"></div>
                        <Calendar />
                        <div className="related-event">
                            <h2>Related Event</h2>
                            <div className="line"></div>
                            {content}
                        </div>
                    </div>
                </div>
                <Modal show={show} onHide={handleClose} testing>
                    <Modal.Header closeButton>
                        <Modal.Title>User Login Required</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Please log in to your account to add to your wishlist</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={() => history.push("/login")}>
                            Login
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }else{
        return <div className="respond-loading">
            <ReactLoading id="loading" type="spinningBubbles" color="#F47A50" height={341} width={150}/>
        </div>
    }
}

export default Detail;
