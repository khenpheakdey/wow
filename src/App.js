import React, { useState , useEffect} from "react";
import Homecontent from "./pages/home-content";
import Admin from "./pages/admin"
import Login from "./pages/login";
import Detail from "./pages/detail"
import { useSelector, useDispatch } from 'react-redux';
import Page404 from "./pages/404.js"
import ShowMore from "./pages/show-more";
import Wishlist from "./components/user/wishlist"
import { Provider } from 'react-redux'
import store from "./store"
import UserInfo from "./components/user/userInfo"
import SecuredRoute from "./components/route/secureRoute"
import AboutUs from "./pages/aboutus"
import user from "./store/user"
import state from "./state"
import PublicRoute from './PublicRoute.js'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {
  const userLogin = useSelector(state => state.user.isLogin);
  console.log(userLogin);
  
  return (
    <Provider store={store}>
      <Router>
        <div style={{ width: "100vw" }}>
          <Switch>
            <PublicRoute exact path="/" component={Homecontent} />
            <PublicRoute exact path="/about" component={AboutUs} />
            <PublicRoute exact path="/login" component={Login} />
            <Route exact path="/admin" component={Admin} />
            <PublicRoute exact path="/event/:id" component={Detail}></PublicRoute>
            <PublicRoute exact path="/event" component={ShowMore}></PublicRoute> 
            <SecuredRoute path="/profile" component={UserInfo}></SecuredRoute> 
            <SecuredRoute path="/wishlist/:userId" component={Wishlist}></SecuredRoute>  
            <Route exact path="*" component={Page404}></Route>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
