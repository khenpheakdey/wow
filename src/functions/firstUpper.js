function firstUpper(text) {
    var result="";
    var i;
    for (i = 0; i < text.length; i++) {
        if(i==0){
            result += text[i].toUpperCase();
        }else{
            result += text[i];
        }
    }
    return result;
}

export default firstUpper;
