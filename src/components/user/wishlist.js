import React, {useEffect, useState} from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import ReactLoading from 'react-loading';

import displayDate from './../../functions/displayDate';
import addFavorite from './../../functions/favorite';

import '../../scss/wishlist.scss';

const Wishlist = () => {
    let content;

    const [isLoading, setLoading] = useState(false);
    const [wishList, setWishList] = useState([])
    
    let { userId } = useParams()
    const history = useHistory();

    const fetchWishlist = async()=>{
        console.log(userId);
        setLoading(true);
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/wishlist/" + userId)
        console.log(response.data);
        if (response.data.length !== 0) {
            setWishList(response.data[0].event)
        }
        setLoading(false);
    }

    const handleAddFavorite = async (id) => {
        const status = await addFavorite(id);

        if (status) {
            fetchWishlist()
        } else {
            alert("failed")
        }
    } 

    useEffect(() => {
        console.log("cat");
        fetchWishlist()
        console.log("bro");
    },[]);


    if(!isLoading) {
        if (wishList.length !== 0) {
            content = wishList.map(event => {
                return(
                    <div className="event-card">
                        <div className="event-img">
                            <div className="img overlay" style={{backgroundImage: `url(${event.image})`}} onClick={()=> history.push("/event/" + event._id)}></div>
                            <button className="add-btn" onClick={() => handleAddFavorite(event._id)}>
                                <ion-icon name="remove-outline"></ion-icon>
                            </button>
                        </div>
                        <div className="event-desc">
                            <h3>{event.title}</h3>
                            <div className="event-details">
                                <div className="event-location">
                                    <ion-icon name="location"></ion-icon>
                                    <p>{event.location}</p>
                                </div>
                                <div className="event-bottom">
                                    <div className="event-date">
                                        <ion-icon name="calendar"></ion-icon>
                                        <p>{displayDate(event.deadline)}</p>
                                    </div>
                                    <div className="category-list">
                                        {event.category.map(category=>{
                                            return (
                                            <div className="event-category">{category}</div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        } else {
            content = 
                <div className="nothing-desc">
                    <p>Save what you like by tapping the nearby heart.<br/>
                    We'll keep 'em safely here for you.</p>
                    <button className="showmore-btn" onClick={()=> history.push({pathname: "/event", state: {type: "recently added"}})}>
                        Go to EVENTS
                    </button>
                </div>
        }
    } else {
        content = <ReactLoading id="loading" type="spinningBubbles" color="#F47A50" height={341} width={150}/>
    }

    return (
        <div className="wishlist-container">
            <h1 className="wishlist-header">MY WISHLIST</h1>
            <div className="wishlist-list">
                <div className="wishlist-events">
                    {content}
                </div>
            </div>
        </div>
    );
}

export default Wishlist;
