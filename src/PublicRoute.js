import React from 'react';
import Header from './components/homepage/header.js'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Footer from './components/homepage/footer.js';

const PublicRoute = ({ component: Component , ...rest})=>{
    return (
        <Route {...rest}  component={(props)=>(
            <div>
                <Header /> {/* HEADER ALWAYS VISIBLE */}
                <Component {...props} />
                <Footer/>
            </div>
        )}
        />
    )
}

export default PublicRoute;