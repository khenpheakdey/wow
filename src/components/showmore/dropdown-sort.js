import { useState } from 'react';
import './../../scss/dropdown-sort.scss'

const DropdownSort = (props) => {

    const [active, setActive] = useState(false);

    const handleOptionChange = (opt) => {
        props.handleDropdownSort(opt)
        setActive(!active);
    }

    const handleDropdown = () => {
        setActive(!active);
    }

    return (
        <div className="sort">
            <h4>Sort by</h4>
            <div className="sort-dropdown">
                <div className="dropdown" onClick={() => handleDropdown()}>
                    <button className="dropdown-btn">{props.dropdownOption}</button>
                    <ion-icon name="caret-down-outline"></ion-icon>
                </div>
                <ul style={{maxHeight: active ? "200px" : "0",  borderBottom: active ? null : "none" }}>
                    <li onClick={() => handleOptionChange("featured")}>Featured</li>
                    <li onClick={() => handleOptionChange("recently added")}>Recently Added</li>
                    <li onClick={() => handleOptionChange("upcoming events")}>Upcoming Events</li>
                </ul>
            </div>
            <div className="breakline"></div>
        </div>
        
    )
}

export default DropdownSort; 