import { React, useRef, useState, useEffect } from 'react';
import ReactLoading from 'react-loading';
import "../../scss/cd_banner.scss"
import displayDate from './../../functions/displayDate';

const Countdown = (props) => {

    let interval = useRef();

    const [day, setDay] = useState("00");
    const [hour, setHour] = useState("00");
    const [minute, setMinute] = useState("00");
    const [second, setSecond] = useState("00");

    const [title, setTitle] = useState("Untitled");
    const [location, setLocation] = useState("Unknown");
    const [date, setDate] = useState("Unknown");

    let eventArray = [];

    props.featureEvent.forEach( event => {
        eventArray.push(event);
        // console.log(eventArray);
    })

    const startTimer = () => {
        if (eventArray.length !== 0) {
            setTitle(eventArray[0].title);
            setLocation(eventArray[0].location);
            setDate(eventArray[0].deadline);

            const countdownDate = new Date(eventArray[0].deadline).getTime();
        
            interval = setInterval(() => {
                const now = new Date().getTime();
                const distance = countdownDate - now;

                const days = Math.abs(Math.floor(distance / (1000 * 60 * 60 * 24)));
                const hours = Math.abs(Math.floor((distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60))) - 6);
                const minutes =  Math.abs(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
                const seconds = Math.abs(Math.floor((distance % (1000 * 60)) / 1000));
                
                if (distance < 0) {
                    clearInterval(interval.current);
                } else {
                    setDay(days);
                    setHour(hours);
                    setMinute(minutes);
                    setSecond(seconds);
                }
            }, 1000);
        }
    }

    useEffect(() => {
        startTimer();
        return () => {
            clearInterval(interval.current);
        };
    });

    let timer;

    if (!props.isLoading) {
        timer = 
        <div className="cd-banner">
            <div className="info">
                <p>Upcoming Event</p>
                <div className="event-title">
                    <h3>{title}</h3>
                </div>
                <div className="event-details">
                    <p>Location: {location}</p>
                    <p>Date: {displayDate(date)}</p>
                </div>
            </div>
            <div className="details-count">
                <div className="countdown">
                    <div className="circle">
                        <h4 className="day">{day}</h4>
                        <h5>Days</h5>
                    </div>
                    <div className="circle">
                        <h4 className="hour">{hour}</h4>
                        <h5>Hours</h5>
                    </div>
                    <div className="circle">
                        <h4 className="minute">{minute}</h4>
                        <h5>Minutes</h5>
                    </div>
                    <div className="circle">
                        <h4 className="second">{second}</h4>
                        <h5>Seconds</h5>
                    </div>
                </div>
            </div> 
        </div>
    } else {
        timer = 
            <div className="loadingBanner">
                <ReactLoading id="loadingCD" type="cylon" color="#F47A50" height={150} width={150} />
            </div>
    }

    return (
        <div style={{justifyContent: props.isLoading ? 'center' : null}}>
            {timer}
        </div>
    )
}

export default Countdown;