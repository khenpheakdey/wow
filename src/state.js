import { createStore } from 'redux';
import userReducer from "./store/user"

const state = createStore(userReducer);

export default state;


