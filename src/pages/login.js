import React, { useState, useEffect } from 'react';
import '../scss/login.scss'
import { useDispatch, useSelector } from "react-redux";
import Loading from "./../components/loading/loading"
import { userLogin, userLogout } from "../store/user";
import axios from "axios"
import store from "../store"
import {
    Redirect,
    useHistory
  } from "react-router-dom";

const Login = () => {
    const history = useHistory()
    const dispatch = useDispatch();
    

    const [userInfoLogin, setuserLogin] = useState({
        email: "",
        password: ""
    })
    const [loading, setloading] = useState(false)

    let [errorLogIn,setErrorLogIn] = useState("")

    const handleLogin = async (e) => {
        e.preventDefault()
        setloading(true)
        try {
            const response = await axios.post("https://jisoo-board.herokuapp.com/auth", userInfoLogin)
            console.log(response + "hello from login");
            localStorage.setItem("token", response.headers.token)
            console.log(response);
        
            setloading(false)
            console.log(response);
            localStorage.setItem("token",response.headers.token)
            localStorage.setItem("user", JSON.stringify(response.data))
            history.replace({pathname:"/profile"})
        }
        catch (e) {
            // console.log(e);
            setloading(false);
            setErrorLogIn(errorContentLogIn(e))
        }
    };

    const checkAuth = ()=>{
        if(localStorage.getItem("token")){
            history.replace({pathname:"/profile"})
        }
    }

    useEffect(() => {
        checkAuth()
    }, []);

    let login = ""

    if (loading) {
        login = null
    }
    else {
        login = <div className="sign-in">
            <h2>SIGN IN {loading}</h2>
            <h3>{errorLogIn}</h3>
            <form onSubmit={handleLogin}>
                <div className="email">
                    <div className="label">
                        <h5>EMAIL  <em>*</em></h5>
                       
                    </div>
                    <input type="text" name="email" onChange={e => setuserLogin(prevState => ({
                        ...prevState,
                        email: e.target.value

                    }))} />
                </div>
                <div className="password">
                    <div className="label">
                        <h5>PASSWORD<em>*</em></h5>
                        
                    </div>
                    <input type="password" name="password" onChange={e => setuserLogin(prevState => ({
                        ...prevState,
                        password: e.target.value

                    }))}/>
                </div>

        
                <button type="submit" className="sign-in-btn">SIGN IN</button>
                
            </form>
        </div>
    }

    let errorContentSignUp= (e) =>{
        return <div className="error-signup">
            <p>Cannot Sign Up. Please try again! <em>*</em></p>
        </div>
    }
     let errorContentLogIn= (e) =>{
        return <div className="error-signup">
            <p>Cannot Sign In. Please try again! <em>*</em></p>
        </div>
    }


    const [userInfoSignin, setuserSignin] = useState({
        name: "", 
        email: "",
        password: ""
    })
    let [error,setError] = useState("")
    const handleSignin = async (e) => {
        e.preventDefault()
        setloading(true)
        try {
            const response = await axios.post("https://jisoo-board.herokuapp.com/api/user", userInfoSignin)
            localStorage.setItem("token", response.headers.token)
            localStorage.setItem("user", JSON.stringify(response.data))
            setloading(false)
            history.push("/")
        }
        catch (e) {
            setloading(false)
            setError(errorContentSignUp(e))
            console.log(e);
        }
    };


    let signup = ""

    if (loading) {
        signup = <Loading loading={loading}></Loading>
    }
    else {
        signup= <div className="sign-in">
            <h2>SIGN UP{loading}</h2>
            <h3>{error}</h3>
            <form onSubmit={handleSignin}>

            <div className="email">
                    <div className="label">
                        <h5>USERNAME <em>*</em></h5>
                        
                    </div>
                    <input type="text" name="email" onChange={e => setuserSignin(prevState => ({
                        ...prevState,
                        name: e.target.value

                    }))} />
                </div>
                <div className="email">
                    <div className="label">
                        <h5>EMAIL<em>*</em></h5>
                        
                    </div>
                    <input type="text" name="email" onChange={e => setuserSignin(prevState => ({
                        ...prevState,
                        email: e.target.value

                    }))} />
                </div>
                <div className="password">
                    <div className="label">
                        <h5>PASSWORD<em>*</em></h5>
                        
                    </div>
                    <input type="password" name="password" onChange={e => setuserSignin(prevState => ({
                        ...prevState,
                        password: e.target.value

                    }))}/>
                </div>

                <button type="submit" className="sign-in-btn">SIGN UP</button>
            </form>
        </div>
    }

    return (
        <div className="login-content" >
            {login}
            <div style={{ borderRight: "0.5px solid #D3D3D3"}}></div>
            {signup}        
        </div>
    )
}

export default Login;