import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "../../scss/userInfo.scss";
import Avatar from "react-avatar";
import firstUpper from "../../functions/firstUpper";
import Pheakdey from "../../assets/pheakdey.jpg";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";
import { Component } from "react";
import { AccountTree } from "@material-ui/icons";
import { fn } from "jquery";
import axios from "axios";

const UserInfo = () => {
  const history = useHistory();
  const checkAuth = () => {
    console.log(localStorage.getItem("token"));
    if (localStorage.getItem("token")) {
      history.replace({ pathname: "/profile" });
    }
  };

  let [user, setUser] = useState(
    JSON.parse(localStorage.getItem("user"))
      ? JSON.parse(localStorage.getItem("user"))
      : {});

  const [accountInfo, setAccountInfo] = useState({
    name: user.name,
    email: user.email,
    firstname: user.firstname,
    lastname: user.lastname,
    address: user.address,
    city: user.city,
    job: user.job,
  });

  const handleUpdateInfo = (key, value) => {
    console.log(key, value);
    setAccountInfo((prevState) => ({
      ...prevState,
      [key]: value,
    }));

    console.log(accountInfo);
  };

  const handleSubmitInfo = async () => {
    setIsDisabled(!isDisabled);
    try {
      const response = await axios.put(
        "https://jisoo-board.herokuapp.com/api/user/" + user._id,
        accountInfo
      );
      console.log(response);
    } catch (e) {
      console.log(e);
    }
  };

  const fetchUpdate = async () => {
    const response = await axios.get(
      "https://jisoo-board.herokuapp.com/api/user/" + user._id
    );
    localStorage.setItem("user", JSON.stringify(response.data));
    console.log(response.data);
    setAccountInfo(response.data);
  };

  useEffect(() => {
    fetchUpdate();
    setUser(
      JSON.parse(localStorage.getItem("user"))
        ? JSON.parse(localStorage.getItem("user"))
        : {}
    );

    console.log(user);
  }, []);

  const [isDisabled, setIsDisabled] = useState(true);

  const EditProfile = () => {
    setIsDisabled(false);

    setAccountInfo({
      name: user.name,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      address: user.address,
      city: user.city,
      job: user.job,
    });
  };

  return (
    <div className="user-container">
      <div className="row">
        <div className="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div className="card card-profile shadow justify-content-center align-items-center">
            
              <div className="profile-img">
                <img
                src={Pheakdey}
                style={{ width: "250px"}}
                className="rounded-circle"
              />
              </div>
        

            <div className="card-body pt-0 pt-md-4">
              <div className="text-center">
                <h3>
                  {accountInfo.name}
                  <span className="font-weight-light">, 27</span>
                </h3>
                <div className="h5 font-weight-300">
                  <i className="ni location_pin mr-2"></i>
                  {accountInfo.city}, Cambodia
                </div>
                <div className="h5 mt-4">
                  <hr className="my-4" />
                  <i className="ni business_briefcase-24 mr-2"></i>Sophomore
                </div>
                <div>
                  <i className="ni education_hat mr-2"></i>
                  {accountInfo.job}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-8 order-xl-1">
          <div className="card bg-secondary shadow">
            <div className="card-header bg-white border-0">
              <div className="row align-items-center">
                <div className="col-8">
                  <h3 className="mb-0">My account</h3>
                </div>
                <div className="col-4 text-right">
                  <a
                    href="#!"
                    className="btn btn-sm btn-primary"
                    onClick={EditProfile}
                  >
                    Edit Profile
                  </a>
                </div>
              </div>
            </div>
            <div className="card-body">
              <form>
                <h6 className="heading-small text-muted mb-4">
                  User information
                </h6>
                <div className="pl-lg-4">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="form-group focused">
                        <label
                          className="form-control-label"
                          for="input-username"
                        >
                          Username
                        </label>
                        <input
                          disabled={isDisabled}
                          type="text"
                          id="input-username"
                          className="form-control form-control-alternative"
                          placeholder="Username"
                          value={accountInfo.name}
                          onChange={(e) =>
                            handleUpdateInfo("name", e.target.value)
                          }
                        />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <label className="form-control-label" for="input-email">
                          Email address
                        </label>
                        <input
                          disabled={isDisabled}
                          type="email"
                          id="input-email"
                          className="form-control form-control-alternative"
                          placeholder="Email"
                          value={accountInfo.email}
                          onChange={(e) =>
                            handleUpdateInfo("email", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="form-group focused">
                        <label
                          className="form-control-label"
                          for="input-first-name"
                        >
                          First name
                        </label>
                        <input
                          disabled={isDisabled}
                          type="text"
                          id="input-first-name"
                          className="form-control form-control-alternative"
                          placeholder={accountInfo.firstname}
                          value={accountInfo.firstname}
                          onChange={(e) =>
                            handleUpdateInfo("firstname", e.target.value)
                          }
                        />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group focused">
                        <label
                          className="form-control-label"
                          for="input-last-name"
                        >
                          Last name
                        </label>
                        <input
                          disabled={isDisabled}
                          type="text"
                          id="input-last-name"
                          className="form-control form-control-alternative"
                          placeholder="Last name"
                          value={accountInfo.lastname}
                          onChange={(e) =>
                            handleUpdateInfo("lastname", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <hr className="my-4" />

                <h6 className="heading-small text-muted mb-4">
                  Contact information
                </h6>
                <div className="pl-lg-4">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group focused">
                        <label
                          className="form-control-label"
                          for="input-address"
                        >
                          Address
                        </label>
                        <input
                          disabled={isDisabled}
                          id="input-address"
                          className="form-control form-control-alternative"
                          placeholder="Home Address"
                          value={accountInfo.address}
                          onChange={(e) =>
                            handleUpdateInfo("address", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-4">
                      <div className="form-group focused">
                        <label className="form-control-label" for="input-city">
                          City
                        </label>
                        <input
                          disabled={isDisabled}
                          type="text"
                          id="input-city"
                          className="form-control form-control-alternative"
                          placeholder="City"
                          value={accountInfo.city}
                          onChange={(e) =>
                            handleUpdateInfo("city", e.target.value)
                          }
                        />
                      </div>
                    </div>

                    <div className="col-lg-4">
                      <div className="form-group">
                        <label
                          className="form-control-label"
                          for="input-country"
                        >
                          University/Work
                        </label>
                        <input
                          disabled={isDisabled}
                          type="text"
                          id="input-job"
                          className="form-control form-control-alternative"
                          placeholder="Occupation"
                          value={accountInfo.job}
                          onChange={(e) =>
                            handleUpdateInfo("job", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <hr className="my-4" />
                <div className="button">
                  <button className="btn-primary" onClick={() => handleSubmitInfo()}>Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      {/* </div> */}
    </div>
  );
};

export default UserInfo;
