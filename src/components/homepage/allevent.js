import React, {useState} from 'react';
import { useHistory } from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';
import ReactLoading from 'react-loading';

import displayDate from './../../functions/displayDate';
import addFavorite from './../../functions/favorite';

import "bootstrap/dist/css/bootstrap.css";
import "../../scss/allevent.scss";

const Allevent = (props) => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const history = useHistory()

    

    const checkWishlist = (event) => {
        return props.wishList.some(evt => evt._id === event._id)
    }
    
    let content
    if(!props.isLoading){
        content = props.featureEvent.map( feature => {
            return(
                <div className="event-card">
                    <div className="event-img">
                        <div className="img overlay" style={{backgroundImage: `url(${feature.image})`}} onClick={()=> history.push("/event/" + feature._id)}></div>
                        <button className="add-btn" style={{borderColor: checkWishlist(feature) ? "tomato" : "white"}}  >
                            <ion-icon name="heart" onClick={(e)=>props.handleAddFavorite(e,feature._id)} style={{color: checkWishlist(feature) ? "tomato" : "white"}}></ion-icon>
                        </button>
                    </div>
                    <div className="event-desc">
                        <h3>{feature.title}</h3>
                        <div className="event-details">
                            <div className="event-location">
                                <ion-icon name="location"></ion-icon>
                                <p>{feature.location}</p>
                            </div>
                            <div className="event-bottom">
                                <div className="event-date">
                                    <ion-icon name="calendar"></ion-icon>
                                    <p>{displayDate(feature.deadline)}</p>
                                </div>
                                <div className="category-list">
                                    {feature.category.map(category=>{
                                        return (
                                        <div className="event-category">{category}</div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    } else {
        content = <ReactLoading id="loading" type="spinningBubbles" color="#F47A50" height={341} width={150}/>
    }

    return (
        <div className="category-modal">
            <div className="event-container">
                <div className="title">
                    <h3>{props.title}</h3>
                    <button className="show-more" onClick={() => history.push({pathname: "/event", state: {type: props.type}})}> 
                        <p> Show more </p>
                        <ion-icon name="chevron-forward-outline"></ion-icon>
                    </button>
                </div>
                <div className="event-content" style={{justifyContent: props.isLoading ? 'center' : 'space-between'}}>
                    {content}
                </div>
                    <Modal show={show} onHide={handleClose} testing>
                        <Modal.Header closeButton>
                            <Modal.Title>User Login Required</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>Please log in to your account to add to your wishlist</Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Cancel
                            </Button>
                            <Button variant="primary" onClick={() => history.push("/login")}>
                                Login
                            </Button>
                        </Modal.Footer>
                    </Modal>
            </div>
        </div>
    )
}

export default Allevent