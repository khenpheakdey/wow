import { createSlice } from '@reduxjs/toolkit'

const user = createSlice({
    name: 'user',
    initialState: {
        isLogin: false,
        userInfo: {}
    },
    reducers: {
        userLogin: (state, action) => {
            state.isLogin = true
            state.userInfo = action.payload
            console.log(action.payload);
        },
        userLogout: (state) => {
            state.isLogin = false
            state.userInfo = {}
        }
    }
})

export const { userLogin, userLogout } = user.actions


export default user.reducer
