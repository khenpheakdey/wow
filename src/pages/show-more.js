import React, { useEffect, useState } from 'react';
import DropdownSort from './../components/showmore/dropdown-sort'
import CategorySelector from './../components/showmore/category-selector'
import { useHistory } from 'react-router-dom';
import ReactLoading from 'react-loading'; 
import displayDate from './../functions/displayDate'
import addFavorite from './../functions/favorite';
import { Modal, Button } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";

import axios from 'axios';
import './../scss/config.scss';
import './../scss/showmore.scss';

const ShowMore = () => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);  

    const history = useHistory();

    const [isLoading, setLoading] = useState(false);
    const [eventList, setEventList] = useState([]);
    const [allEvents, setAllEvents] = useState([]);
    const [featureEvent, setFeatureEvent] = useState([]);
    const [wishList, setWishList] = useState([])
    const [dropdownOption, setDropdownOption] = useState(history.location.state !== undefined ? history.location.state.type : "recently added")

    const tempCategories = [];
    let fetchedCategory = [];
    let categoryQuery;
    let userInfo = JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")) : {
        _id: "", 
        name:"", 
        email:""
    }

    const fetchAllEvent = async () => {
        // setLoading(true)
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/event")
        setEventList(response.data);
        setAllEvents(response.data);
        // setLoading(false)
    }

    const fetchFeature = async () => {
        // setLoading(true)
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/event/feature")
        setFeatureEvent(response.data);
        // setLoading(false)
    }

    const fetchCategories = async () => {
        // fetch categories from api
        setLoading(true);
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/category");
        fetchedCategory = response.data;

        // map each categories with isActive: false
        fetchedCategory.forEach(event => {
            tempCategories.push({
                "title": event,
                "isActive": false
            })
        })
        setLoading(false);
    }

    // set categoryList to the tempCategories map
    const [categoryList, setCategoryList] = useState(tempCategories);

    const setCategoryQuery = async (e) => {
        categoryQuery = e;
        
        setLoading(true);
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/event?" + categoryQuery);
        setEventList(response.data);
        setLoading(false);
    }

    const handleDropdownSort = (sortType) => {
        setDropdownOption(sortType)
        let sortEvent = [...allEvents];      
        switch(sortType) {
            case "featured":
                setEventList(featureEvent);
                break;
                
            case "recently added":    
                sortEvent.sort((a, b) => { return new Date(b.date) - new Date(a.date) })
                setEventList(sortEvent)
                break;
                
            case "upcoming events":
                let upcomingEvents = [];
                upcomingEvents = sortEvent.filter(a => new Date(a.deadline) - new Date() > 0)
                upcomingEvents.sort((a, b) => { return new Date(b.deadline) - new Date(a.deadline) }).reverse();
                setEventList(upcomingEvents)
                break;

            default:
                alert("Oh no");
                break;
        }
    } 

    const fetchWishlist = async()=>{
        setLoading(true);
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/wishlist/" + userInfo._id)
        setWishList(response.data[0].event)
        setLoading(false);
    }
    
    const handleAddFavorite = async (e, id) => {
        if(e.target.style.color === "tomato"){
            console.log("tomato");
            e.target.style.color = "white"
        }
        else {
            console.log("white");
            e.target.style.color = "tomato"
        }
        const status = await addFavorite(id);

        console.log(e.target.style.color);
        if (status) {
            console.log("object");
        } else {
            handleShow();
            e.target.style.color = "white"
        }
    }

    const checkWishlist = (event) => {
        return wishList.some(evt => evt._id === event._id)
    }
    
    useEffect(() => {
        fetchAllEvent();
        fetchCategories();
        fetchFeature();
        fetchWishlist();
    }, [])

    let content;

    if (!isLoading) {
        content = eventList.map((feature, index) => {
            return (
                <div className="event-card">
                    <div className="event-img">
                        <div className="img overlay" style={{backgroundImage: `url(${feature.image})`}} onClick={()=> history.push("/event/" + feature._id)}></div>
                        <button className="add-btn" onClick={(e) => handleAddFavorite(e, feature._id)}>
                            <ion-icon name="heart" style={{color: checkWishlist(feature) ? "tomato" : "white"}}></ion-icon>
                        </button>
                    </div>
                    <div className="event-desc">
                        <h3>{feature.title}</h3>
                        <div className="event-details">
                            <div className="event-location">
                                <ion-icon name="location"></ion-icon>
                                <p>{feature.location}</p>
                            </div>
                            <div className="event-bottom">
                                <div className="event-date">
                                    <ion-icon name="calendar"></ion-icon>
                                    <p>{displayDate(feature.deadline)}</p>
                                </div>
                                <div className="category-list">
                                    {feature.category.map(category => {
                                        return (
                                            <div className="event-category">{category}</div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    } else {
        content = 
            <div className="respond-loading">
                <ReactLoading id="loading" type="spinningBubbles" color="#F47A50" height={341} width={150}/>
            </div>
    }

    return (
        <div className="showmore-container">
            <div className="left-container">
                <div className="filter">
                    <DropdownSort 
                        dropdownOption = {dropdownOption}
                        setDropdownOption = {setDropdownOption}
                        handleDropdownSort = {handleDropdownSort}
                    />
                    <CategorySelector
                        categoryList={categoryList}
                        setCategoryList={setCategoryList}
                        isLoading={isLoading}
                        categoryQuery={categoryQuery}
                        setCategoryQuery={setCategoryQuery}
                    />
                </div>
            </div>
            <div className="right-container">
                <div className="event-container" style={{filter: isLoading ? "opacity(0.5)" : "none"}}>
                    {content}
                </div>
            </div>
            <Modal show={show} onHide={handleClose} testing>
                <Modal.Header closeButton>
                    <Modal.Title>User Login Required</Modal.Title>
                </Modal.Header>
                <Modal.Body>Please log in to your account to add to your wishlist</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={() => history.push("/login")}>
                        Login
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default ShowMore;


