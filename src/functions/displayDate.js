function displayDate(date) {
    let monthList = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let setDate = new Date(date);
    let d = setDate.getDate();
    let m = setDate.getMonth();
    let y = setDate.getFullYear();

    if (monthList === undefined || d === NaN || y === NaN) {
        return " ";
    } else {
        return monthList[m] + " " + d + ", " + y;
    }
    
}

export default displayDate;
