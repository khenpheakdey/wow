import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import logo_white from "../../assets/logo_white.svg"
import "../../scss/footer.scss";

const Footer = () => {

    const history = useHistory();

    return (
        <footer>
            <div className="footer-container">
                <div className="desc">
                    <img src={logo_white} alt="logo"/>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipiscing elit, praesent 
                        sagittis cum quis dapibus elementum luctus, blandit eros integer 
                        nec phasellus ridiculus. Ad vitae tempor cum morbi class laoreet 
                        viverra luctus senectus, scelerisque inceptos mattis turpis libero 
                        dapibus consequat ac, elementum facilisi at mollis facilisis netus
                        id cubilia.
                    </p>
                </div>
                <div className="links">
                    <div>
                        <h4>QUICK LINKS</h4>
                        <div className="links-row">
                            <ul>
                                <li>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                    <a href="​​​​​/">Home</a>
                                </li>
                                <li>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                    <a href="/about">About Us</a>
                                </li>
                                <li>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                    <a onClick={() => history.push({pathname: "/event", state: {type: "featured"}})}>Featured Event</a>
                                </li>
                                <li>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                    <a onClick={() => history.push({pathname: "/event", state: {type: "recently added"}})}>Recently Added</a>
                                </li>
                                <li>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                    <a onClick={() => history.push({pathname: "/event", state: {type: "upcoming events"}})}>Upcoming Events</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="contact">
                    <h4>CONTACT US</h4>
                    <div className="contact-info">
                        <div className="call">
                            <ion-icon name="call"></ion-icon>
                            <a href="">(+855) 23 996 111</a>
                        </div>
                        <div className="mail">
                            <ion-icon name="mail"></ion-icon>
                            <a href="">work@eventboard.com</a>
                        </div>
                        <div className="location">
                            <ion-icon name="location" id="location"></ion-icon>
                            <a href="">No. 8, St. 315, Boeng Kak 1, Tuol Kork, <br></br> Phnom Penh, Cambodia</a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="policy">
                <p>2021 © EventBoard. Privacy Policy</p>
            </div>
        </footer>
    )
}

export default Footer;
