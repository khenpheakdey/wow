import React from 'react'
import '../../src/scss/aboutus.scss'
import Pheakdey from "../assets/pheakdey.jpg";
import Panharith from "../assets/panharith.jpg";
import Menghang from "../assets/menghang.jpg";
import Vuthi from "../assets/vuthi.jpg";
import Working from "../assets/Working.svg";
import { useHistory } from 'react-router-dom';


const AboutUs = () => {
    const history = useHistory();

    return (
        <div className="about-container">
            <div className="about-header">
                <div className="left-image">
                    <h5>ABOUT OUR TEAM</h5>
                <h2>They can do, We can do!</h2>
                <div className="breakline"></div>
                <div className="contact-team">
                    <ion-icon name="logo-facebook"></ion-icon>
                    <ion-icon name="logo-linkedin"></ion-icon>
                    <ion-icon name="logo-twitter"></ion-icon>
                </div>
                </div>
                <div className="about-image">
                    <img src={Working} alt="working"/>
                </div>
            </div>
            <div className="about">
            <div className="about-heading-title">
                <h5>THE MOST POWERFUL TEAM ON EARTH LOL</h5>
                <h2>We are a team full of positivities xD</h2>
                <div className="short-des">
                    <p>Lorem ipsum dolor sit amet, consec tetur adipiscing the egtlit sekido eiusmod of the tempor incid dunt ulert labore et dolore all magna aliqua mi bibendum neque egestas.</p>
                </div>
            </div>
            <div className="about-team">
                <div className="team-member" onClick={(e)  => {
                    e.preventDefault();
                    window.open('https://www.facebook.com/rithy.viravuthi.7');
                    }}>
                <div className="team-wrap">
                    <div className="bg-image" style={{backgroundImage: `url(${Vuthi}`,backgroundPosition: '50%',backgroundSize: 'cover', backgroundRepeat: 'no-repeat'}}>
                        <div className="about-left">
                            <div className="about-body">
                                <div className="body-top"></div>
                                    <div className="body">
                                    <div className="style">
                                    </div>
                                    <div className="about-info">
                                        <div className="name">
                                        <h2>Viravuthi Rithy Top</h2>
                                    </div>
                                    <div className="role">
                                        <h2>The Collector</h2>
                                    </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div className="about-right">
                        </div>
                    </div>
                </div>
            </div>
                <div className="team-member" onClick={(e)  => {
                    e.preventDefault();
                    window.open('https://facebook.com/kdeykhen');
                    }}>
                <div className="team-wrap">
                    <div className="bg-image" style={{backgroundImage: `url(${Pheakdey}`,backgroundPosition: '50%',backgroundSize: 'cover', backgroundRepeat: 'no-repeat'}}>
                        <div className="about-left">
                            <div className="about-body">
                                <div className="body-top"></div>
                                    <div className="body">
                                    <div className="style">
                                    </div>
                                    <div className="about-info">
                                        <div className="name">
                                        <h2>Pheakdey  <br></br>Khen</h2>
                                    </div>
                                    <div className="role">
                                        <h2>The Small UI</h2>
                                    </div>
                                    </div>
                                </div>
                            
                            </div>
                    </div>
                    <div className="about-right">
                        </div>
                    </div>
                </div>
            </div>
            <div className="team-member" onClick={(e)  => {
                    e.preventDefault();
                    window.open('https://facebook.com/menghang.hean');
                    }}>
                <div className="team-wrap">
                    <div className="bg-image" style={{backgroundImage: `url(${Menghang}`, backgroundPosition: '0% 10%',
                    backgroundPositionY: '50%',
                    backgroundSize: 'cover', backgroundRepeat: 'no-repeat'}}>
                        <div className="about-left">
                            <div className="about-body">
                                <div className="body-top"></div>
                                    <div className="body">
                                    <div className="style">
                                    </div>
                                    <div className="about-info">
                                        <div className="name">
                                        <h2>Menghang Hean</h2>
                                    </div>
                                    <div className="role">
                                        <h2>The API guy</h2>
                                    </div>
                                    </div>
                                </div>
                            
                            </div>
                    </div>
                    <div className="about-right">
                        </div>
                    </div>
                </div>
            </div>
            <div className="team-member" onClick={(e)  => {
                    e.preventDefault();
                    window.open('https://facebook.com/panharith.sun.5');
                    }}>
                <div className="team-wrap">
                    <div className="bg-image" style={{backgroundImage: `url(${Panharith}`,
                backgroundPosition: '80% 10%',backgroundSize: 'cover', backgroundRepeat: 'no-repeat'}}>
                        <div className="about-left">
                            <div className="about-body">
                                <div className="body-top"></div>
                                    <div className="body">
                                    <div className="style">
                                    </div>
                                    <div className="about-info">
                                        <div className="name">
                                        <h2>Panharith Sun</h2>
                                    </div>
                                    <div className="role">
                                        <h2>The Function</h2>
                                    </div>
                                    </div>
                                </div>
                            
                            </div>
                    </div>
                    <div className="about-right">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    )
}

export default AboutUs;