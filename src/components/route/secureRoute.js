import React, { useEffect } from "react";
import state from "../../state";
import { useSelector, useDispatch } from "react-redux";
import Header from '../homepage/header'
import Footer from '../homepage/footer';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";

const SecuredRoute = ({ isAuth, component: Component, ...rest }) => {
  const history = useHistory();
  const userLogin = useSelector((state) => state.user.isLogin);
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      history.push("/");
    }
  }, []);
  return (
    <Route
      {...rest}
      render={(props) => {
        if (localStorage.getItem("token")) {
          return <div>
            <Header/>
            <Component {...props} />
            <Footer></Footer>
          </div>;
        } else {
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />;
        }
      }}
    />
  );
};

export default SecuredRoute;
