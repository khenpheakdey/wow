import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from "axios"

import ModalEvent from "../components/homepage/modal"
import Countdown from "../components/homepage/cd_banner"
import Allevent from "../components/homepage/allevent"

import addFavorite from './../functions/favorite';

import "bootstrap/dist/css/bootstrap.css";

const Content = () => {

    const history = useHistory();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [isLoading, setLoading] = useState()
    const [show, setShow] = useState(false);
    const [status, setStatus] = useState(false); 
    const [feature, setFeature] = useState([])
    const [recentEvents, setRecentEvents] = useState([]);
    const [upcomingEvents, setUpcomingEvents] = useState([]);
    const [allEvent, setallEvent] = useState([])
    const [wishList, setWishList] = useState([])

    let userInfo = JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")) : {
        _id: "",
        name: "",
        email: ""
    }

    const fetchFeature = async () => {
        setLoading(true)
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/event/feature")
        setFeature(response.data.slice(0, 3))
        setLoading(false)
        // fetchRecentEvent();
    }

    const fetchRecentEvent = (events) => {
        setLoading(true);
        let recentTemp = events.sort((a, b) => { return new Date(b.date) - new Date(a.date) });
        setRecentEvents(recentTemp.slice(0, 3));
        setLoading(false)
    }

    const fetchUpcomingEvent = (events) => {
        setLoading(true);
        let upcomingTemp = events.filter(a => new Date(a.deadline) - new Date() > 0)
        setUpcomingEvents(upcomingTemp);
        setLoading(false);
    }

    const fetchAllEvent = async () => {
        let tempEvents;

        setLoading(true)
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/event")
        setallEvent(response.data.slice(0, 8))
        tempEvents = response.data;
        fetchRecentEvent(tempEvents);
        fetchUpcomingEvent(tempEvents);
        setLoading(false)
    }

    const fetchWishlist = async()=>{
        setLoading(true);
        const response = await axios.get("https://jisoo-board.herokuapp.com/api/wishlist/" + userInfo._id)
        if (response.data.length !== 0) {
            setWishList(response.data[0].event)
        }
        setLoading(false);
    }

    const handleAddFavorite = async (e, id) => {
        if(e.target.style.color === "tomato"){
            console.log("tomato");
            e.target.style.color = "white"
        }
        else {
            console.log("white");
            e.target.style.color = "tomato"
        }
        const status = await addFavorite(id);

        console.log(e.target.style.color);
        if (status) {
            console.log("object");
        } else {
            handleShow();
            e.target.style.color = "white"
        }
    }  

    const ModalBox = () => {
        return (
            <>
                <Modal show={show} onHide={handleClose} testing>
                    <Modal.Header closeButton>
                        <Modal.Title>User Login Required</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Please log in to your account to add to your wishlist</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={() => history.push("/login")}>
                            Login
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        )
    }

    useEffect(() => {
        fetchFeature()
        fetchAllEvent()
        fetchWishlist()
    }, [])

    return (
        <div className="category-content">
            <ModalEvent
                isLoading={isLoading}
                title={"FEATURED EVENTS"}
                type={"featured"}
                desc={"See the popular events that are happening soon."}
                featureEvent={feature}
                wishList={wishList}
                handleAddFavorite={handleAddFavorite}
            ></ModalEvent>
            <ModalEvent
                isLoading={isLoading}
                title={"RECENTLY ADDED"}
                type={"recently added"}
                desc={"See the events that are just recently added."}
                featureEvent={recentEvents}
                wishList={wishList}
                handleAddFavorite={handleAddFavorite}
            ></ModalEvent>
            <Countdown
                featureEvent={upcomingEvents}
                isLoading={isLoading}
            />
            <Allevent
                isLoading={isLoading}
                title={"ALL EVENT"}
                type={"upcoming events"}
                featureEvent={allEvent}
                wishList={wishList}
                handleAddFavorite = {handleAddFavorite}
            ></Allevent>
            <ModalBox/>
        </div>
    )
}

export default Content