import { useEffect, useState } from 'react';
import Checkbox from 'react-custom-checkbox';
import ReactLoading from 'react-loading';
import './../../scss/category-selector.scss';
import './../../scss/modal.scss';
// import useLocalStorage from 'react-use-localstorage';

const CategorySelector = (props) => {
    let param = new URLSearchParams(window.location.search)

    // const [items, setItems] = useLocalStorage('name', 'Initial Value');

    var categoriesArr = [...props.categoryList];

    const initCategoriesArr = () => {
        // Display the key/value pairs
        for(var pair of param.entries()) {
            const tempIndex = pair[0].slice(-2, -1);
            categoriesArr[tempIndex].isActive = true;
            // console.log(categoriesArr[tempIndex].isActive);
        }
    }

    const handleCategoryClick = (index, value) => {
        // updates the category array with the changed isActive value
        const cate = categoriesArr[index];
        cate.isActive = value;
        props.setCategoryList(categoriesArr);
        
        if (value) {
            param.append(`category[${index}]`, cate.title);
        } else {
            param.delete(`category[${index}]`);
        }
        
        props.setCategoryQuery(param.toString());

        // this line just replace the url without reloading the page
        window.history.replaceState({}, '', `?${param}`);
    }

    return (
        <div className="category-selector">
            <h4>Category</h4>
            <div className="categories">
                {/* {!props.isLoading ?  */}
                {
                    categoriesArr.map((cata, index) => {
                        return (
                            <div className="category-item">
                                <Checkbox 
                                    cursor="pointer"
                                    checked={props.categoryList[index].isActive}
                                    icon={
                                        <div style={{
                                            backgroundColor: "#f47a50",
                                            padding: "5px",
                                            borderRadius: "2px"
                                        }}>
                                        </div>
                                    }
                                    size={15}
                                    borderColor={props.categoryList[index].isActive ? "#f47a50" : "#8a8a8a"}
                                    borderWidth={1.5}
                                    borderRadius={3}
                                    label={cata.title}
                                    labelStyle={{
                                        marginLeft: "0.5rem",
                                        fontSize: "0.9rem",
                                        color: "grey",
                                        userSelect: "none",
                                        cursor: "pointer"
                                    }}
                                    onChange={(value) => 
                                        handleCategoryClick(index, value)
                                    }
                                />
                            </div>
                        )
                    })
                    // : <div style={{height: "350px",display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
                    //     {/* <ReactLoading id="loading" type="spinningBubbles" color="#F47A50" width={150}/> */}
                    // </div>
                }
            </div>
        </div>
    )
}

export default CategorySelector;