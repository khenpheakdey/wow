import React from 'react';
import { Component } from 'react';
import '../scss/404.scss';
import Lottie from 'react-lottie';
import error from '../404.json';
import { useHistory } from 'react-router-dom';
import fixing from '../fixing.json';

const Page404= () =>{
    const history = useHistory();

    return (
        <div className="error-container">
            <div className="error-text">
                <div className="error-wrapper">
                    <div className="glitch-error" data-text="404">
                        <Lottie options={{animationData: error}} height={400}
                        width={400} style={{margin: 0}}/>
                    </div>
                    <h4>Error Page Not Found</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                    <button className="goback-btn" onClick={()=>
                        history.push("/")
                    }>
                        HomePage
                    </button>
                    
                </div>
            </div>
            <div className="error-animation">
                <Lottie options={{animationData: fixing}} height={800} width={900} style={{margin: 0}}/>
            </div>
        </div>
    );
    
};

export default Page404;