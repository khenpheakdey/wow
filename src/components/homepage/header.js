import React, { useEffect, useState } from "react";
import { HistoryOutlined, HistoryRounded } from "@material-ui/icons";
import { BrowserRouter, Link } from "react-router-dom";
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios'

import "bootstrap/dist/css/bootstrap.css";

import logo from "../../assets/logo.png";
import Login from "../../pages/login.js";
import DropdownMenu from "./dropdown_pf";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";

import "../../scss/header.scss";
import user from "../../store/user";
import Wishlist from "../user/wishlist";

function Header(props) {

  const history = useHistory();
	let content;

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);  
  
  const [show, setShow] = useState(false);
  const [wishList, setWishList] = useState([])
  const [loading, setLoading] = useState(false);
  
  let userInfo = JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")) : {
    _id: "",
    name: "",
    email: ""
  }

  const fetchWishlist = async() => {
    setLoading(true);
    const response = await axios.get("https://jisoo-board.herokuapp.com/api/wishlist/" + userInfo._id)
    if (response.data.length !== 0) {
        setWishList(response.data[0].event)
        // if (response.data[0].event.length !== 0) {
        //   setShow(true)
        // }
    }
    setLoading(false);
  }

  useEffect(() => {
    fetchWishlist();
  }, [wishList])
  
  const checkAuth = () => {
    if (localStorage.getItem("token")) {
      history.push('/wishlist/' + userInfo._id)
    } else {
      handleShow(); 
    }
  };

  if (localStorage.getItem("token")) {
    content = (
      <div className="logged-in">
        <Link className="link-profile" to={{ pathname: "/login" }}>
          <ion-icon
            name="person-circle-outline"
            className="profile-icon"
          ></ion-icon>
        </Link>
        <ul className="drop-item">
          <li>
            <a href="/login">
              <ion-icon name="person-outline"></ion-icon>Profile
            </a>
          </li>
          <li>
            <a
              href=""
              onClick={() => {
                localStorage.removeItem("token");
                localStorage.removeItem("user");
              }}
            >
              <ion-icon name="log-out-outline"></ion-icon>LogOut
            </a>
          </li>
        </ul>
      </div>
    );
  } else {
    content = (
      <Link className="link-profile" to={{ pathname: "/login" }}>
        <ion-icon name="log-in-outline" className="profile-icon"></ion-icon>
      </Link>
    );
  }
  
  const ModalBox = () => {
    return (
      <Modal show={show} onHide={handleClose} testing>
          <Modal.Header closeButton>
              <Modal.Title>User Login Required</Modal.Title>
          </Modal.Header>
          <Modal.Body>Please log in to your account to add to your wishlist</Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                  Cancel
              </Button>
              <Button variant="primary" onClick={() => history.push("/login")}>
                  Login
              </Button>
          </Modal.Footer>
      </Modal>
    )
  }

  return props.url === "*" ? null : (
    <>
      <header>
        <div className="top-banner">
          <Link to={{ pathname: "/" }}>
            <img src={logo} alt="" />
          </Link>
          <ul className="nav">
            <li>
              <div className="profile">{content}</div>
            </li>
            <li onClick={() => checkAuth()}>
                <div className="link favorite">
                  <ion-icon name="heart-outline"></ion-icon>
                  <div className="number-indicator" style={{display: wishList.length !== 0 ? "flex" : "none"}}>
                    <p>{wishList.length}</p>
                  </div>
                </div>
            </li>
          </ul>
        </div>

        <div className="poster">
          <h3>EVENTBOARD</h3>
          <p>Where your interests gather</p>
        </div>

        <div className="category">
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About Us</a>
            </li>
          </ul>
        </div>
      </header>
      <ModalBox/>
    </>
  );
}

export default Header;
